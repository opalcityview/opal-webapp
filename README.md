# opal-webapp

# GIỚI THIỆU

Opal Web Application là ứng dụng web nhúng để theo dõi toàn bộ thông tin về dự án Opal Cityview do tập đoàn Đất Xanh phát triển.

Ứng dụng đang trong quá trình xây dựng và hoàn thiện, dự kiến sẽ tung bản ver-0.1 vào tháng 08/2021

# CHỨC NĂNG APP

- Theo dõi thông tin đự án
- Cập nhật tiến độ dự án
- Xem tình trạng giỏ hàng
- Giao lưu với các Sales của sàn khác
- Nhúng kênh youtube để theo dõi video dự án

# LIÊN HỆ

- Địa chỉ: Đại lộ Bình Dương, Phú Thọ, Thủ Dầu Một, Bình Dương
- Hotline: 0938279155
- Email: duanopalcityview@gmail.com
